# Sequence comparison and hierarchical clustering in Java

This projects consists in performing hierarchical clustering on biological sequences.
We start from the gene hemoglobin on several species and compare theire respective nucleotide sequences and amino-acids sequences.

![Cluster of hemoglobin genes](./images/hemoglobin-genes.png)

## Todo

- Hemoglobin has undergone an [interesting evolution history](https://en.wikipedia.org/wiki/Hemoglobin#Evolution_of_vertebrate_hemoglobin).
    - [ ] add a small paragraph explaining the duplication between hemoglobin alpha and beta, and how alpha was subsequently duplicated again, explaining the a1, a2, a3...
    - notice that for mice (_Mus musculus_) and rats (_Rattus norvegicus_) a1 and a2 are more similar within a species than between species, but they are all more similar among them than with rats' a3.
    - [ ] **Todo:** add the sequences for Hbb for mice and rats and see the resulting clustering
- [ ] challenge: provide a mysterious species' Hba sequence and use clustering for providing an estimation of the kind of species it belongs to

